(function (exports,Chartist,moment) {
'use strict';

moment = 'default' in moment ? moment['default'] : moment;

var BackendConnector = (function () {
    function BackendConnector() {
        this.host = 'https://api.sensation.io';
        this.iframeAuthentication = { initialized: false };
    }
    BackendConnector.prototype.setHost = function (host) {
        this.host = host;
    };
    BackendConnector.prototype.authenticateApp = function (clientId, tenantId, redirectUri, scopes, callback) {
        if (this.iframeAuthentication.frame) {
            try {
                document.body.removeChild(this.iframeAuthentication.frame);
                this.iframeAuthentication.frame = undefined;
            }
            catch (err) {
                console.log('[SIO]: WARN: Could not remove SIO auth components', err);
            }
        }
        if (this.iframeAuthentication.authListener) {
            try {
                window.removeEventListener('message', this.iframeAuthentication.authListener);
                this.iframeAuthentication.authListener = undefined;
            }
            catch (err) {
                console.log('[SIO]: WARN: Could not remove SIO auth listener', err);
            }
        }
        this.iframeAuthentication.uri = redirectUri;
        var scopesString = "";
        scopes.forEach(function (val) {
            if (scopesString !== "")
                scopesString += "%20";
            scopesString += val;
        });
        this.createAuthorizationFrame(this.host + "/v2/auth?client_id=" + clientId + "&redirect_uri=" + redirectUri + "&tenant_id=" + tenantId + "&scope=" + scopesString);
        window.addEventListener('message', function (event) {
            var data = event.data;
            if (data && data.length > 0) {
                var tokenIndex = data.indexOf('token');
                if (tokenIndex >= 0) {
                    var token = data.substr(tokenIndex + 6, data.length - tokenIndex);
                    callback(undefined, token);
                }
            }
        }, false);
    };
    BackendConnector.prototype.createAuthorizationFrame = function (authUrl) {
        var _this = this;
        this.iframeAuthentication.frame = document.createElement('iframe');
        var versionPostfix = '';
        this.iframeAuthentication.frame.setAttribute('src', authUrl);
        this.iframeAuthentication.frame.setAttribute('style', 'display:none;');
        document.body.appendChild(this.iframeAuthentication.frame);
        this.iframeAuthentication.frame.onload = function () {
            _this.iframeAuthentication.initialized = true;
        };
    };
    BackendConnector.prototype.getStats = function (token, questionId, tagFilter, callback) {
        $.ajax({
            url: this.host + '/v2/stats/questions/' + questionId + (tagFilter && tagFilter.length > 0 ? '?tag=' + tagFilter : ''),
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + token);
            }
        }).done(function (stats) {
            if (callback) {
                callback(undefined, stats);
            }
            else
                console.error('[BACKENDCONNECTOR]: Callback was not provided');
        }).fail(function (loaderr) {
            console.log('[BACKENDCONNECTOR]: There was an error loading the question stats', loaderr);
            callback(loaderr);
        });
    };
    BackendConnector.prototype.getTagStats = function (token, questionId, callback) {
        $.ajax({
            url: this.host + '/v2/stats/questions/' + questionId + "/tags",
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + token);
            }
        }).done(function (tagStats) {
            if (callback) {
                callback(undefined, tagStats);
            }
            else
                console.error('[BACKENDCONNECTOR]: Callback was not provided');
        }).fail(function (loaderr) {
            console.log('[BACKENDCONNECTOR]: There was an error loading the question stats', loaderr);
            callback(loaderr);
        });
    };
    BackendConnector.prototype.loadQuestion = function (token, questionId, callback) {
        $.ajax({
            url: this.host + '/v2/questions/' + questionId,
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + token);
            }
        }).done(function (question) {
            if (callback) {
                callback(undefined, question);
            }
            else
                console.error('[BACKENDCONNECTOR]: Callback was not provided');
        }).fail(function (loaderr) {
            console.log('[BACKENDCONNECTOR]: There was an error loading the poll question', loaderr);
            callback(loaderr);
        });
    };
    BackendConnector.prototype.addRecord = function (token, record, callback) {
        $.ajax({
            url: this.host + '/v2/records/' + record.questionId,
            type: 'POST',
            headers: {
                "Content-Type": "application/json; charset=utf8"
            },
            data: JSON.stringify({
                question_id: record.questionId,
                x: record.x,
                y: record.y,
                tags: record.tags,
                key: record.key
            }),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', "Bearer " + token);
            }
        }).done(function (response) {
            if (callback) {
                callback(undefined, response);
            }
            else {
                console.log('[BACKENDCONNECTOR]: Callback was not provided');
            }
        }).fail(function (loaderr) {
            callback(loaderr);
            console.log('[BACKENDCONNECTOR]: There was an error when posting a multi jam', loaderr);
        });
    };
    return BackendConnector;
}());

var HTMLRenderer = (function () {
    function HTMLRenderer() {
        this.configuration = {
            categories: {},
            emotionMapDimension: { x: 10, y: 10 },
            emotionMapSquareSize: 25,
            backgroundColor: { r: 255, g: 255, b: 255, a: 0.9 },
            gridMode: "dashed",
            gridColor: { r: 200, g: 200, b: 200, a: 1 },
            gridCrosshairColor: { r: 160, g: 160, b: 160, a: 1 },
            gridCrosshairMode: "line",
            readonly: false,
            submitOnClick: false
        };
        this.defaultCategoryId = "defaultCategory";
        this.defaultCategoryName = "Default Category";
        this.emotionMapElement = undefined;
        this.data = {};
        this.onConfigurationChangeCallbacks = [];
        this.onHoverCallbacks = [];
        this.onClickCallbacks = [];
        this.onSubmitCallbacks = [];
    }
    HTMLRenderer.prototype.extendConfiguration = function (configuration) {
        this.configuration = $.extend({}, this.configuration, configuration);
        if (configuration.labels)
            this.renderLabels();
        if (configuration.emotionMapSquareSize || configuration.backgroundColor || configuration.gridColor || configuration.gridMode || configuration.gridColor || configuration.gridCrosshairMode || configuration.gridCrosshairColor)
            this.initializeMap(this.configuration.targetElementId);
        if (configuration.targetElementId)
            this.setEmotionMapElementById(configuration.targetElementId);
        this.triggerConfigurationChangeCallbacks();
    };
    HTMLRenderer.prototype.getConfiguration = function () {
        return this.configuration;
    };
    HTMLRenderer.prototype.getCategory = function (categoryId) {
        return this.configuration.categories[categoryId];
    };
    HTMLRenderer.prototype.onConfigurationChange = function (callback, triggerOnRegistration) {
        if (triggerOnRegistration === void 0) { triggerOnRegistration = true; }
        this.onConfigurationChangeCallbacks.push(callback);
        if (triggerOnRegistration)
            callback(this.configuration);
    };
    HTMLRenderer.prototype.triggerConfigurationChangeCallbacks = function () {
        var _this = this;
        $.each(this.onConfigurationChangeCallbacks, function (index, fn) {
            fn(_this.configuration);
        });
    };
    HTMLRenderer.prototype.setText = function (text, quarter, extraCssClasses) {
        var textContainerClass = "emotionmap-text-" + quarter;
        this.emotionMapElement.find("." + textContainerClass).fadeOut(function () {
            this.remove();
        });
        this.emotionMapElement.append($("<div class=\"emotionmap-text " + textContainerClass + " " + extraCssClasses + "\" style=\"width:" + (this.configuration.emotionMapDimension.x * this.configuration.emotionMapSquareSize) / 2 + "px;height:" + ((this.configuration.emotionMapDimension.y * this.configuration.emotionMapSquareSize) / 2 + 1) + "px\">" + text + "</div>").hide().fadeIn());
    };
    HTMLRenderer.prototype.setAfterRegisterMessage = function (html, extraCssClasses) {
        var textContainerClass = "emotionmap-text-center";
        this.emotionMapElement.find("." + textContainerClass).fadeOut(function () {
            this.remove();
        });
        this.emotionMapElement.append($("<div class=\"emotionmap-text " + textContainerClass + " " + extraCssClasses + "\" style=\"width:" + (this.configuration.emotionMapDimension.x * this.configuration.emotionMapSquareSize) + "px;height:" + ((this.configuration.emotionMapDimension.y * this.configuration.emotionMapSquareSize) + 1) + "px\"></div>").append(html).hide().fadeIn());
    };
    HTMLRenderer.prototype.clearTexts = function () {
        this.emotionMapElement.find(".emotionmap-text").fadeOut(function () {
            this.remove();
        });
    };
    HTMLRenderer.prototype.setLabels = function (top, bottom, left, right) {
        this.configuration.labels = { top: top, bottom: bottom, left: left, right: right };
        this.renderLabels();
        this.triggerConfigurationChangeCallbacks();
    };
    HTMLRenderer.prototype.addPoints = function (points) {
        var _this = this;
        $.each(points, function (index, point) {
            _this.addPoint(point);
        });
    };
    HTMLRenderer.prototype.addCategory = function (category) {
        this.configuration.categories[category.id] = category;
        this.triggerConfigurationChangeCallbacks();
    };
    HTMLRenderer.prototype.addPoint = function (pointOrCategory, x, y, value) {
        var point;
        if (typeof pointOrCategory === "string")
            point = { x: x, y: y, value: value, categoryId: pointOrCategory };
        else
            point = pointOrCategory;
        if (point.categoryId === undefined)
            point.categoryId = this.getDefaultCategory().id;
        if (this.data[point.categoryId] === undefined)
            this.data[point.categoryId] = [];
        this.data[point.categoryId].push(point);
    };
    HTMLRenderer.prototype.convertYXArrayToPoints = function (yxArray) {
        var data = [];
        for (var y = 0; y < yxArray.length; y++)
            for (var x = 0; x < yxArray[y].length; x++) {
                data.push({ x: x, y: y, value: yxArray[y][x] });
            }
        return data;
    };
    HTMLRenderer.prototype.convertXYArrayToPoints = function (xyArray) {
        var data = [];
        for (var y = 0; y < xyArray.length; y++)
            for (var x = 0; x < xyArray[y].length; x++) {
                data.push({ x: x, y: y, value: xyArray[x][y] });
            }
        return data;
    };
    HTMLRenderer.prototype.showLoading = function (targetElementId, show) {
        if (show === void 0) { show = true; }
        var target = $("#" + this.configuration.targetElementId);
        if (targetElementId)
            target = $("#" + targetElementId);
        target.toggleClass("emotionmap-loading", show);
    };
    HTMLRenderer.prototype.showRandomData = function () {
        this.clearData(this.getDefaultCategory().id);
        var data = [];
        for (var x = 0; x < this.configuration.emotionMapDimension.x; x++)
            for (var y = 0; y < this.configuration.emotionMapDimension.y; y++)
                if (Math.random() > 0.5)
                    data.push({ x: x, y: y, value: Math.random(), categoryId: this.getDefaultCategory().id });
        this.addPoints(data);
        this.renderPoints();
        this.triggerConfigurationChangeCallbacks();
    };
    HTMLRenderer.prototype.clearHeatMap = function () {
        this.clearHeatMapByTargetElement(this.emotionMapElement[0]);
    };
    HTMLRenderer.prototype.clearHeatMapByTargetElement = function (targetElement) {
        $(targetElement).find("td").css("backgroundColor", this.getBackgroundColorAsRGBAString());
        this.renderPoints();
    };
    HTMLRenderer.prototype.clearHover = function () {
        this.clearHeatMap();
    };
    HTMLRenderer.prototype.onHover = function (callback) {
        this.onHoverCallbacks.push(callback);
    };
    HTMLRenderer.prototype.triggerOnHover = function (x, y, categoryId, event) {
        if (categoryId === undefined || categoryId === null)
            categoryId = this.getDefaultCategory().id;
        $.each(this.onHoverCallbacks, function (index, fn) {
            fn(x, y, categoryId, event);
        });
        this.clearHover();
        var firstColor = this.getFirstColor(categoryId);
        var secondColor = this.getSecondColor(categoryId);
        this.paintColor(x, y, firstColor, true);
        this.paintColor(x + 1, y, secondColor);
        this.paintColor(x - 1, y, secondColor);
        this.paintColor(x, y - 1, secondColor);
        this.paintColor(x, y + 1, secondColor);
        this.paintColor(x + 1, y + 1, secondColor);
        this.paintColor(x + 1, y - 1, secondColor);
        this.paintColor(x - 1, y + 1, secondColor);
        this.paintColor(x - 1, y - 1, secondColor);
    };
    HTMLRenderer.prototype.onClick = function (callback) {
        this.onClickCallbacks.push(callback);
    };
    HTMLRenderer.prototype.triggerOnClick = function (x, y, categoryId, rawEvent) {
        this.clearHover();
        if (categoryId === undefined || categoryId === null)
            categoryId = this.getDefaultCategory().id;
        if (!this.data[categoryId])
            this.data[categoryId] = [];
        var category = this.getCategory(categoryId);
        var firstColor = this.getFirstColor(categoryId);
        this.emotionMapElement.find("*[data-x=\"" + x + "\"][data-y=\"" + y + "\"]").css("backgroundColor", firstColor);
        if (category.maxVotes !== undefined && this.data[categoryId].length >= category.maxVotes)
            this.data[categoryId].shift();
        this.data[categoryId].push({ categoryId: categoryId, x: x, y: y, value: 1 });
        if (category.changeableVote === false && (category.maxVotes === undefined || this.data[categoryId].length >= category.maxVotes))
            this.configuration.readonly = true;
        this.renderPoints();
        this.triggerConfigurationChangeCallbacks();
        $.each(this.onClickCallbacks, function (index, fn) {
            fn(x, y, categoryId, rawEvent);
        });
        if (this.configuration.submitOnClick)
            this.triggerOnSubmit(x, y, categoryId);
    };
    HTMLRenderer.prototype.onSubmit = function (callback) {
        this.onSubmitCallbacks.push(callback);
    };
    HTMLRenderer.prototype.triggerOnSubmit = function (x, y, categoryId) {
        if (categoryId === undefined || categoryId === null)
            categoryId = this.getDefaultCategory().id;
        $.each(this.onSubmitCallbacks, function (index, fn) {
            fn(x, y, categoryId);
        });
        if (this.configuration.afterSubmit) {
            switch (this.configuration.afterSubmit.type) {
                case "message":
                    this.setText(this.configuration.afterSubmit.data.text, this.configuration.afterSubmit.data.quarter);
                    break;
                case "heatmap":
                    console.error("which heatmap shall I show...");
                    break;
            }
        }
    };
    HTMLRenderer.prototype.initializeMap = function (targetElementId) {
        var _this = this;
        this.onConfigurationChangeCallbacks = [];
        this.onHoverCallbacks = [];
        this.onClickCallbacks = [];
        this.onSubmitCallbacks = [];
        this.clearAllData();
        if (targetElementId)
            this.configuration.targetElementId = targetElementId;
        var target = $("#" + this.configuration.targetElementId);
        var html = "<div class=\"emotionmap-container\" id=\"" + this.configuration.targetElementId + "\" style=\"width:" + this.configuration.emotionMapSquareSize * this.configuration.emotionMapDimension.x + "px\"><table class=\"emotionmap-table\">";
        for (var y = 0; y < this.configuration.emotionMapDimension.y; y++) {
            html += "<tr>";
            for (var x = 0; x < this.configuration.emotionMapDimension.x; x++) {
                var style = "width:" + this.configuration.emotionMapSquareSize + "px;height:" + this.configuration.emotionMapSquareSize + "px;";
                style += "border:1px " + this.getBorderStyle(this.configuration.gridMode) + " " + this.getRGBAColorString(this.configuration.gridColor) + ";";
                if (x === ((this.configuration.emotionMapDimension.x - 2) / 2))
                    style += "border-right-color:" + this.getRGBAColorString(this.configuration.gridCrosshairColor) + ";border-right-style:" + this.getBorderStyle(this.configuration.gridCrosshairMode) + ";";
                if (y === ((this.configuration.emotionMapDimension.y - 2) / 2))
                    style += "border-bottom-color: " + this.getRGBAColorString(this.configuration.gridCrosshairColor) + ";border-bottom-style:" + this.getBorderStyle(this.configuration.gridCrosshairMode) + ";";
                html += "<td class=\"emotionmap-point\" data-x=\"" + x + "\" data-y=\"" + y + "\" style=\"" + style + "\"></td>";
            }
            html += "</td>";
        }
        html += "</table>";
        html += "<div class=\"emotionmap-label emotionmap-label-top\"></div>";
        html += "<div class=\"emotionmap-label emotionmap-label-bottom\"></div>";
        html += "<div class=\"emotionmap-label emotionmap-label-left\"></div>";
        html += "<div class=\"emotionmap-label emotionmap-label-right\"></div>";
        html += "</div>";
        var emotionmap = $(html);
        emotionmap.on("click", function (event) {
            if (!_this.configuration.readonly) {
                event.preventDefault();
                event.stopImmediatePropagation();
                _this.triggerOnClick(parseInt(event.target["dataset"]["x"]), parseInt(event.target["dataset"]["y"]), null, event);
            }
        });
        emotionmap.on("mouseover", function (event) {
            if (!_this.configuration.readonly) {
                event.preventDefault();
                event.stopImmediatePropagation();
                _this.triggerOnHover(parseInt(event.target["dataset"]["x"]), parseInt(event.target["dataset"]["y"]), null, event);
            }
        });
        emotionmap.on("touchend", function (event) {
            if (!_this.configuration.readonly) {
                event.preventDefault();
                event.stopImmediatePropagation();
                var currentTouchLocation = event["originalEvent"].changedTouches[0];
                var realTarget = document.elementFromPoint(currentTouchLocation.clientX, currentTouchLocation.clientY);
                _this.triggerOnClick(parseInt(realTarget["dataset"]["x"]), parseInt(realTarget["dataset"]["y"]), null, event);
            }
        });
        emotionmap.on("touchmove", function (event) {
            if (!_this.configuration.readonly) {
                event.preventDefault();
                event.stopImmediatePropagation();
                var currentTouchLocation = event["originalEvent"].changedTouches[0];
                var realTarget = document.elementFromPoint(currentTouchLocation.clientX, currentTouchLocation.clientY);
                _this.triggerOnHover(parseInt(realTarget["dataset"]["x"]), parseInt(realTarget["dataset"]["y"]), null, event);
            }
        });
        this.emotionMapElement = emotionmap;
        target.fadeOut(function () {
            target.replaceWith(emotionmap);
            _this.renderLabels();
            emotionmap.hide().fadeIn(function () {
                _this.renderPoints();
            });
        });
    };
    HTMLRenderer.prototype.getBorderStyle = function (style) {
        switch (style) {
            case "dashed":
                return "dashed";
            case "line":
                return "solid";
            case "dotted":
            default:
                return "dotted";
        }
    };
    HTMLRenderer.prototype.renderLabels = function () {
        if (this.configuration && this.configuration.labels && this.emotionMapElement) {
            this.emotionMapElement.find(".emotionmap-label.emotionmap-label-top").text(this.configuration.labels.top);
            this.emotionMapElement.find(".emotionmap-label.emotionmap-label-bottom").text(this.configuration.labels.bottom);
            this.emotionMapElement.find(".emotionmap-label.emotionmap-label-left").text(this.configuration.labels.left);
            this.emotionMapElement.find(".emotionmap-label.emotionmap-label-right").text(this.configuration.labels.right);
        }
    };
    HTMLRenderer.prototype.convertHexToRGB = function (colorAsHex) {
        colorAsHex = colorAsHex.replace('#', '');
        if (colorAsHex.length !== 6 && colorAsHex.length !== 8)
            throw new Error("Please provide hex colors in 6-digit format!");
        var r = parseInt(colorAsHex.substring(0, 2), 16);
        var g = parseInt(colorAsHex.substring(2, 4), 16);
        var b = parseInt(colorAsHex.substring(4, 6), 16);
        return { r: r, g: g, b: b };
    };
    HTMLRenderer.prototype.convertHexToRGBA = function (colorAsHex) {
        if (colorAsHex === '')
            return;
        var rgb = this.convertHexToRGB(colorAsHex);
        colorAsHex = colorAsHex.replace('#', '');
        var a;
        colorAsHex.length === 8 ? a = parseInt(colorAsHex.substring(6, 8), 16) / 255 : a = 1;
        return $.extend(rgb, { a: a });
    };
    HTMLRenderer.prototype.getColor = function (categoryId, alphaChannel) {
        if (alphaChannel === void 0) { alphaChannel = 1; }
        var category = this.configuration.categories[categoryId];
        if (!category)
            throw new Error("Could not find category '" + categoryId + "'!");
        return this.getRGBAColorString($.extend(category.foregroundColor, { a: alphaChannel }));
    };
    HTMLRenderer.prototype.getRGBAColorString = function (color) {
        return "rgba(" + color.r + ", " + color.g + ", " + color.b + ", " + color.a + ")";
    };
    HTMLRenderer.prototype.getBackgroundColorAsRGBAString = function () {
        return "rgba(" + this.configuration.backgroundColor.r + ", " + this.configuration.backgroundColor.g + ", " + this.configuration.backgroundColor.b + ", " + this.configuration.backgroundColor.a + ")";
    };
    HTMLRenderer.prototype.getFirstColor = function (category) {
        return this.getColor(category, 1);
    };
    HTMLRenderer.prototype.getSecondColor = function (category) {
        return this.getColor(category, 0.2);
    };
    HTMLRenderer.prototype.renderPoints = function () {
        var _this = this;
        this.emotionMapElement.find("td").css("backgroundColor", this.getBackgroundColorAsRGBAString());
        $.each(this.data, function (index, dataSet) {
            for (var i = 0; i < dataSet.length; i++) {
                _this.emotionMapElement.find("td[data-x=\"" + dataSet[i].x + "\"][data-y=\"" + dataSet[i].y + "\"]").css("backgroundColor", _this.getColor(dataSet[i].categoryId, dataSet[i].value));
            }
        });
    };
    HTMLRenderer.prototype.clearData = function (categoryId) {
        this.data[categoryId] = [];
    };
    HTMLRenderer.prototype.clearAllData = function () {
        this.data = {};
    };
    HTMLRenderer.prototype.getDataForCategoryId = function (categoryId) {
        return this.data[categoryId];
    };
    HTMLRenderer.prototype.getData = function () {
        return this.data;
    };
    HTMLRenderer.prototype.createDefaultCategory = function () {
        var category = {
            id: this.defaultCategoryId,
            foregroundColor: { r: 228, g: 40, b: 57 },
            label: this.defaultCategoryName,
            maxVotes: 1,
            changeableVote: true
        };
        this.configuration.categories[this.defaultCategoryId] = category;
        this.data[category.id] = [];
        this.triggerConfigurationChangeCallbacks();
        return category;
    };
    HTMLRenderer.prototype.paintColor = function (x, y, color, force) {
        if (force === void 0) { force = false; }
        var element = this.emotionMapElement.find("*[data-x=\"" + x + "\"][data-y=\"" + y + "\"]");
        if (force || this.isFieldEmpty(x, y))
            element.css("backgroundColor", color);
    };
    HTMLRenderer.prototype.isFieldEmpty = function (x, y) {
        for (var category in this.data) {
            for (var point in this.data[category]) {
                if (this.data[category][point].x === x && this.data[category][point].y === y) {
                    return false;
                }
            }
        }
        return true;
    };
    HTMLRenderer.prototype.getEmotionMapElement = function () {
        return this.emotionMapElement;
    };
    HTMLRenderer.prototype.setEmotionMapElementById = function (id) {
        this.emotionMapElement = $("#" + id);
    };
    HTMLRenderer.prototype.getDefaultCategory = function () {
        if (this.configuration.categories[this.defaultCategoryId] === undefined)
            return this.createDefaultCategory();
        return this.configuration.categories[this.defaultCategoryId];
    };
    HTMLRenderer.prototype.authorize = function (clientId, tenantId, uri, callback) {
        var SIOPlugin = window["SIOPlugin"];
        SIOPlugin = { 'initialized': false };
        var initialized = false;
        if (SIOPlugin.frame) {
            try {
                document.body.removeChild(SIOPlugin.frame);
                SIOPlugin.frame = undefined;
            }
            catch (err) {
                console.log('[SIO]: WARN: Could not remove SIO auth components', err);
            }
        }
        if (SIOPlugin.authListener) {
            try {
                window.removeEventListener('message', SIOPlugin.authListener);
                SIOPlugin.authListener = undefined;
            }
            catch (err) {
                console.log('[SIO]: WARN: Could not remove SIO auth listener', err);
            }
        }
        SIOPlugin.uri = uri;
        var authUrl = 'https://rest.sensation.io/token?client_id=' + clientId + '&redirect_uri=' + uri + '&tenant=' + tenantId;
        SIOPlugin.createFrame = function () {
            SIOPlugin.frame = document.createElement('iframe');
            var versionPostfix = '';
            SIOPlugin.frame.setAttribute('src', authUrl);
            SIOPlugin.frame.setAttribute('style', 'display:none;');
            document.body.appendChild(SIOPlugin.frame);
            SIOPlugin.frame.onload = function () {
                SIOPlugin.initialized = true;
            };
        };
        SIOPlugin.createFrame();
        window.addEventListener('message', function (event) {
            var data = event.data;
            if (data && data.length > 0) {
                var tokenIndex = data.indexOf('sio_token');
                if (tokenIndex >= 0) {
                    var token = data.substr(tokenIndex + 10, data.length - tokenIndex);
                    callback(undefined, token);
                }
            }
        }, false);
    };
    return HTMLRenderer;
}());

var Utils = (function () {
    function Utils() {
    }
    Utils.getParameterByName = function (name, url) {
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
        var results = regex.exec(url);
        if (!results) {
            return null;
        }
        if (!results[2]) {
            return '';
        }
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    };
    return Utils;
}());

var StoryBuilder = (function () {
    function StoryBuilder(storyContainer, htmlRenderer, backendConnector) {
        this.currentStep = 0;
        this.resultSubmitted = false;
        this.controls = {};
        this.globalTags = [];
        this.onStoryNodeLeaveCallbacks = [];
        this.onStoryNodeBackCallbacks = [];
        this.storyContainer = storyContainer;
        if (this.storyContainer.length === 0)
            throw new Error("StoryContainer could not be found!");
        this.htmlRenderer = htmlRenderer;
        this.backendConnector = backendConnector;
    }
    StoryBuilder.prototype.getShortDateString = function (year, month, week, day) {
        var months = [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"
        ];
        var dateString = "";
        if (year != null) {
            if (month != null) {
                if (day != null) {
                    dateString = months[month] + " " + day;
                }
                else {
                    dateString = months[month];
                }
            }
            else if (week != null) {
                var date = new Date(year, 0, (1 + (week - 1) * 7));
                dateString = months[date.getMonth()] + " " + date.getDate();
            }
            else {
                dateString = "" + year;
            }
        }
        else {
            console.error('Parameter "year" must be specified to generate date string');
        }
        return dateString;
    };
    StoryBuilder.prototype.aggregateSeries = function (rawSeries) {
        var series = [];
        var xsum = 0;
        var ysum = 0;
        var count = 0;
        var xscore = 0;
        var yscore = 0;
        var heartbeat = 0;
        var date;
        for (var i = 0; i < rawSeries.length; i++) {
            xsum += rawSeries[i].xsum;
            ysum += rawSeries[i].ysum;
            count += rawSeries[i].jam_count;
            xscore = 100.0 * (xsum / (1.0 * count)) / 9.0;
            yscore = 100.0 * (9.0 - ysum / (1.0 * count)) / 9.0;
            heartbeat = (xscore + yscore) / 2.0;
            if (rawSeries[i].year != null) {
                if (rawSeries[i].month != null) {
                    if (rawSeries[i].day != null) {
                        date = new Date(rawSeries[i].year, rawSeries[i].month, rawSeries[i].day);
                    }
                    else {
                        date = new Date(rawSeries[i].year, rawSeries[i].month);
                    }
                }
                else if (rawSeries[i].week != null) {
                    date = new Date(rawSeries[i].year, 0, (1 + (rawSeries[i].week - 1) * 7));
                }
                else {
                    console.log('[DEBUG]: Could not resolve date from entry', rawSeries[i]);
                }
            }
            series.push({
                x: date,
                y: heartbeat
            });
        }
        return series;
    };
    StoryBuilder.prototype.loadStoryFromURL = function (endpointUrl, readyCallback) {
        var _this = this;
        this.tokenHandler(function (token) {
            $.ajax({
                type: "GET",
                url: endpointUrl,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', "Bearer " + token);
                }
            }).done(function (response) {
                if (response && response.schema) {
                    _this.story = {
                        tenantId: response.tenant_id,
                        name: response.name,
                        nodes: []
                    };
                    if (response.schema.tile_color) {
                        try {
                            var tileColor = _this.htmlRenderer.convertHexToRGB(response.schema.tile_color);
                            if (tileColor && tileColor.r) {
                                _this.htmlRenderer.getDefaultCategory().foregroundColor = tileColor;
                            }
                        }
                        catch (jcErr) {
                            console.error('Failed to load story color', jcErr);
                        }
                    }
                    if (response.schema.client_logo) {
                        $(".client-logo").attr("src", response.schema.client_logo);
                    }
                    if (response.schema.tags_prefix)
                        _this.setGlobalTagPrefix(response.schema.tags_prefix);
                    if (response.schema.key)
                        _this.setGlobalKey(response.schema.key);
                    if (response.schema.views && response.schema.intro_captions) {
                        $.each(response.schema.views, function (index, elem) {
                            if (elem.id === "intro") {
                                var node = {
                                    type: "view",
                                    animated: elem.animated,
                                    texts: response.schema.intro_captions,
                                    backgroundURL: elem.background_image,
                                    cssClasses: response.schema.intro_classes,
                                    controls: {
                                        next: {
                                            text: "Start"
                                        }
                                    }
                                };
                                _this.story.nodes.push(node);
                            }
                        });
                    }
                    $.each(response.schema.story, function (index, nodeElement) {
                        console.log('[DEBUG]: NODE', {
                            nodeElement: nodeElement
                        });
                        switch (nodeElement.type) {
                            case "list":
                                var tagsNode = {
                                    type: "tags",
                                    multiSelect: false,
                                    questionId: nodeElement.question_id,
                                    subTitle: nodeElement.tags_title,
                                    tagsPrefix: nodeElement.tags_prefix,
                                    tags: $.map(nodeElement.list_items, function (val) { return val.tag; }),
                                    maxSelect: 1,
                                    initiallyDisabled: true,
                                    fixedPosition: nodeElement.fixed_position,
                                    asGlobalTags: true,
                                    backgroundURL: nodeElement.background_image,
                                    mixpanel_descriptor: nodeElement.mixpanel_descriptor
                                };
                                _this.story.nodes.push(tagsNode);
                                break;
                            case "tags":
                                var nodeTags = [];
                                var tokenized = false;
                                if (nodeElement.token_tags && nodeElement.token_tags.length > 0) {
                                    tokenized = true;
                                    nodeTags = nodeElement.token_tags;
                                }
                                else {
                                    nodeTags = nodeElement.sentiment_tags;
                                }
                                var multiTagsNode = {
                                    type: "tags",
                                    multiSelect: true,
                                    tokenized: tokenized,
                                    questionId: nodeElement.question_id,
                                    title: nodeElement.tags_subtitle,
                                    tagsPrefix: nodeElement.tags_prefix,
                                    tags: $.map(nodeTags, function (val) { return val.tag; }),
                                    maxSelect: nodeElement.max_select,
                                    initiallyDisabled: true,
                                    fixedPosition: nodeElement.fixed_position,
                                    backgroundURL: nodeElement.background_image,
                                    mixpanel_descriptor: nodeElement.mixpanel_descriptor
                                };
                                if (nodeElement.max_select)
                                    multiTagsNode.subTitle = "Please choose no more than " + nodeElement.max_select + "!";
                                _this.story.nodes.push(multiTagsNode);
                                break;
                            case "text":
                                var textNode = {
                                    type: "text",
                                    questionId: nodeElement.question_id,
                                    title: nodeElement.title,
                                    tagsPrefix: nodeElement.tags_prefix,
                                    initiallyDisabled: false,
                                    fixedPosition: nodeElement.fixed_position,
                                    backgroundURL: nodeElement.background_image,
                                    mixpanel_descriptor: nodeElement.mixpanel_descriptor
                                };
                                _this.story.nodes.push(textNode);
                                break;
                            case "analytics":
                                var analyticsNode = {
                                    type: "analytics",
                                    questionId: nodeElement.question_id,
                                    title: nodeElement.title,
                                    backgroundURL: nodeElement.background_image,
                                    disableMap: nodeElement.disable_map,
                                    disableTags: nodeElement.disable_tags,
                                    disableTimeline: nodeElement.disable_timeline,
                                    disableHeartbeat: nodeElement.disable_heartbeat,
                                    disableAxesScores: nodeElement.disable_axes_scores,
                                    tagPrefix: nodeElement.tag_prefix,
                                    maxTags: nodeElement.max_tags,
                                    fixedPosition: nodeElement.fixed_position,
                                    sendResultsOnEnter: nodeElement.submit_result,
                                    mixpanel_descriptor: nodeElement.mixpanel_descriptor
                                };
                                _this.story.nodes.push(analyticsNode);
                                break;
                            case "map":
                                var mapNode = {
                                    type: "map",
                                    questionId: nodeElement.question_id,
                                    title: nodeElement.title,
                                    result: {
                                        questionId: nodeElement.question_id,
                                        key: nodeElement.key
                                    },
                                    fixedPosition: nodeElement.fixed_position,
                                    backgroundURL: nodeElement.background_image,
                                    mixpanel_descriptor: nodeElement.mixpanel_descriptor
                                };
                                _this.story.nodes.push(mapNode);
                                break;
                            case "custom":
                                var customNode = {
                                    type: "custom",
                                    selector: nodeElement.selector,
                                    showLogo: nodeElement.show_logo,
                                    fixedPosition: nodeElement.fixed_position,
                                    backgroundURL: nodeElement.background_image,
                                    mixpanel_descriptor: nodeElement.mixpanel_descriptor,
                                    controls: {
                                        next: {
                                            text: nodeElement.next_text || "Next"
                                        }
                                    }
                                };
                                _this.story.nodes.push(customNode);
                                break;
                            case "exit":
                                var exitNode = {
                                    type: "view",
                                    animated: false,
                                    sendResultsOnEnter: true,
                                    controls: {
                                        next: {
                                            hidden: true
                                        },
                                        previous: {
                                            hidden: true
                                        }
                                    },
                                    texts: response.schema.exit_captions,
                                    fixedPosition: nodeElement.fixed_position,
                                    backgroundURL: nodeElement.background_image,
                                    cssClasses: response.schema.exit_classes,
                                    mixpanel_descriptor: nodeElement.mixpanel_descriptor
                                };
                                _this.story.nodes.push(exitNode);
                                break;
                        }
                    });
                    if (typeof readyCallback === "function")
                        readyCallback();
                }
                else {
                    console.error('Could not load story schema. Please verify that the schema ID is correct.', response);
                }
            });
        });
    };
    StoryBuilder.prototype.start = function (startStep) {
        if (startStep === void 0) { startStep = 0; }
        this.reset();
        this.currentStep = startStep;
        if (this.story.nodes[this.currentStep])
            this.renderStep(this.currentStep);
        else
            console.error("No story step '" + this.currentStep + "' available!");
    };
    StoryBuilder.prototype.reset = function () {
        this.currentStep = 0;
        this.globalTags = [];
        if (this.story) {
            for (var node in this.story.nodes) {
                this.story.nodes[node].data = {};
                if (this.story.nodes[node].type === "map") {
                    this.story.nodes[node].result = {};
                }
            }
        }
    };
    StoryBuilder.prototype.updateStepsCounterContent = function () {
        var currentStep = this.currentStep + 1;
        var totalSteps = this.story.nodes.length;
        if (!(this.stepsToSkipInCounter instanceof Array))
            this.controls.stepsCounter.get(0).innerText = currentStep + " / " + totalSteps;
        else if (this.stepsToSkipInCounter.indexOf(currentStep) !== -1)
            this.controls.stepsCounter.get(0).innerText = "";
        else {
            totalSteps -= this.stepsToSkipInCounter.length;
            $.each(this.stepsToSkipInCounter, function (index, value) {
                if (value < currentStep)
                    currentStep--;
            });
            this.controls.stepsCounter.get(0).innerText = currentStep + " / " + totalSteps;
        }
    };
    StoryBuilder.prototype.renderStep = function (position) {
        this.updateStepsCounterContent();
        if (this.controls.next)
            this.controls.next.toggleClass("disabled", !this.hasNextStep());
        if (this.controls.previous)
            this.controls.previous.toggleClass("disabled", !this.hasPreviousStep());
        this.renderStoryNode(this.story.nodes[position]);
    };
    StoryBuilder.prototype.renderStoryNode = function (node) {
        var _this = this;
        mixpanel.track(node.mixpanel_descriptor);
        console.log('[DEBUG]: RENDERSTORYNODE', node);
        $(".client-logo").hide();
        this.storyContainer.fadeOut(function () {
            _this.storyContainer.empty();
            window.scrollTo(0, 0);
            var stepControls = _this.controls;
            var globalTags = _this.globalTags;
            $(".emotionmap-title").empty();
            $(".emotionmap-subtitle").empty();
            if (node.title) {
                if (!$(".emotionmap-title")) {
                    _this.storyContainer.append("<div class=\"emotionmap-title\">" + node.title + "</div>");
                }
                else {
                    $(".emotionmap-title").append(node.title);
                }
            }
            if (node.subTitle && node.type != "text")
                _this.storyContainer.append("<div class=\"emotionmap-subtitle\">" + node.subTitle + "</div>");
            if (_this.controls) {
                if (_this.controls.next) {
                    if (node.controls && node.controls.next && node.controls.next.text) {
                        _this.controls.next.text(node.controls.next.text);
                    }
                    else
                        _this.controls.next.text("Next");
                }
                if (_this.controls.next) {
                    if (node.controls && node.controls.next && node.controls.next.hidden === true)
                        _this.controls.next.fadeOut();
                    else
                        _this.controls.next.fadeIn();
                }
                if (_this.controls.previous) {
                    if (node.controls && node.controls.previous && node.controls.previous.hidden === true)
                        _this.controls.previous.fadeOut();
                    else
                        _this.controls.previous.fadeIn();
                }
            }
            if (node.sendResultsOnEnter) {
                if (!_this.resultSubmitted) {
                    _this.sendResults();
                }
                else {
                    console.log('Results have already been submitted.');
                }
            }
            _this.storyContainer.css('position', node.fixedPosition === true ? 'fixed' : 'inherit');
            _this.storyContainer.css('background-image', node.backgroundURL ? 'url(' + node.backgroundURL + ')' : "none");
            switch (node.type) {
                case "tags":
                    var tagsNode_1 = node;
                    var tagCoordinates = void 0;
                    var selectedTags_1;
                    if (tagsNode_1.initiallyDisabled && stepControls && stepControls.next) {
                        stepControls.next.addClass("disabled");
                    }
                    if (tagsNode_1.questionId) {
                        for (var i = 0; i < _this.story.nodes.length; i++) {
                            if (_this.story.nodes[i].type === "map" && _this.story.nodes[i].questionId === tagsNode_1.questionId) {
                                var storyMapNode_1 = _this.story.nodes[i];
                                tagCoordinates = storyMapNode_1.result;
                                if (!(storyMapNode_1.result.tags instanceof Array))
                                    storyMapNode_1.result.tags = [];
                                selectedTags_1 = storyMapNode_1.result.tags;
                                break;
                            }
                        }
                    }
                    else if (tagsNode_1.asGlobalTags === undefined || tagsNode_1.asGlobalTags === true) {
                        selectedTags_1 = _this.globalTags;
                    }
                    else
                        selectedTags_1 = [];
                    var jqueryObjects_1 = [];
                    for (var index in tagsNode_1.tags) {
                        var classes = "btn btn-default btn-sm btn-tags";
                        var tag = tagsNode_1.tags[index];
                        var tagLabel = tagsNode_1.tags[index];
                        if (tag.indexOf("header|") < 0) {
                            if (tagsNode_1.tokenized && tagCoordinates) {
                                var axis = void 0;
                                var tokenNeg = void 0;
                                var tokenPos = void 0;
                                var tokenExp = void 0;
                                var tokenParts = void 0;
                                var tokenExpStart = tag.indexOf("[");
                                var tokenExpEnd = tag.indexOf("]");
                                if (tokenExpStart >= 0 && tokenExpEnd > 0) {
                                    tokenExp = tag.substr(tokenExpStart, tokenExpEnd - tokenExpStart + 1);
                                    tokenParts = tokenExp.split("|");
                                    if (tokenParts.length == 3) {
                                        axis = tokenParts[0].replace('[', '').replace(']', '');
                                        tokenNeg = tokenParts[1].replace('[', '').replace(']', '');
                                        tokenPos = tokenParts[2].replace('[', '').replace(']', '');
                                        if (axis == "y") {
                                            if (tagCoordinates.y < 5) {
                                                tag = tag.replace(tokenExp, tokenPos);
                                            }
                                            else {
                                                tag = tag.replace(tokenExp, tokenNeg);
                                            }
                                        }
                                        else {
                                            if (tagCoordinates.x < 5) {
                                                tag = tag.replace(tokenExp, tokenNeg);
                                            }
                                            else {
                                                tag = tag.replace(tokenExp, tokenPos);
                                            }
                                        }
                                        tagsNode_1.tags[index] = tag;
                                        tagLabel = tag;
                                    }
                                    else {
                                    }
                                }
                            }
                            if (tagsNode_1.tagsPrefix)
                                tag = tagsNode_1.tagsPrefix + "::" + tag;
                            if (selectedTags_1.indexOf(tag) !== -1)
                                classes += " btn-primary";
                            jqueryObjects_1[index] = $("<button class=\"" + classes + "\" data-index=\"" + index + "\">" + tagLabel + "</button>");
                            jqueryObjects_1[index].on("click", function () {
                                var dataIndex = $(this).attr("data-index");
                                var dataTag = tagsNode_1.tags[dataIndex];
                                if (tagsNode_1.tagsPrefix)
                                    dataTag = tagsNode_1.tagsPrefix + "::" + dataTag;
                                var selectedIndex = selectedTags_1.indexOf(dataTag);
                                if (selectedIndex === -1) {
                                    if (tagsNode_1.maxSelect) {
                                        if (tagsNode_1.maxSelect === 1) {
                                            $.each(jqueryObjects_1, function (index, element) {
                                                if (element.hasClass("btn-primary")) {
                                                    var delIndex = selectedTags_1.indexOf(tagsNode_1.tagsPrefix + "::" + element[0].innerText);
                                                    selectedTags_1.splice(delIndex, 1);
                                                }
                                                element.toggleClass("btn-primary", false);
                                            });
                                        }
                                        else if (selectedTags_1.length >= tagsNode_1.maxSelect) {
                                            console.info("already selected maximum of tags!");
                                            return;
                                        }
                                    }
                                    jqueryObjects_1[dataIndex].toggleClass("btn-primary", true);
                                    selectedTags_1.push(dataTag);
                                }
                                else {
                                    jqueryObjects_1[dataIndex].toggleClass("btn-primary", false);
                                    selectedTags_1.splice(selectedIndex, 1);
                                }
                                if (tagsNode_1.initiallyDisabled && stepControls && stepControls.next) {
                                    if (selectedTags_1 && selectedTags_1.length > 0) {
                                        stepControls.next.removeClass("disabled");
                                    }
                                    else {
                                        stepControls.next.addClass("disabled");
                                    }
                                }
                                var gtIndex = null;
                                if (tagsNode_1.asGlobalTags) {
                                    if (globalTags) {
                                        gtIndex = globalTags.indexOf(dataTag);
                                        if (gtIndex === -1) {
                                            if (tagsNode_1.maxSelect && tagsNode_1.maxSelect === 1) {
                                                $.each(tagsNode_1.tags, function (gti, gtElement) {
                                                    var delIndex = globalTags.indexOf(tagsNode_1.tagsPrefix + "::" + gtElement);
                                                    if (delIndex >= 0) {
                                                        globalTags.splice(delIndex, 1);
                                                    }
                                                });
                                            }
                                            globalTags.push(dataTag);
                                        }
                                        else {
                                            globalTags.splice(gtIndex, 1);
                                        }
                                    }
                                }
                            });
                        }
                        else {
                        }
                    }
                    if (typeof tagsNode_1.onLeave === "function")
                        _this.onStoryNodeLeaveCallbacks.push(function () { tagsNode_1.onLeave(selectedTags_1); });
                    var wrapperClasses = "tags-wrapper";
                    if (tagsNode_1.tags.length <= 8) {
                        wrapperClasses += " tags-list";
                    }
                    var wrapperElement = $("<div class=\"" + wrapperClasses + "\"></div>");
                    wrapperElement.append(jqueryObjects_1);
                    _this.storyContainer.append(wrapperElement);
                    if (typeof node.onStart === "function") {
                        node.onStart(_this.storyContainer, function () {
                            _this.storyContainer.fadeIn();
                        });
                    }
                    else {
                        _this.storyContainer.fadeIn();
                    }
                    break;
                case "analytics":
                    var analyticsNode = node;
                    _this.renderAnalyticsStep(analyticsNode, '');
                    break;
                case "text":
                    var textNode_1 = node;
                    var textValue_1;
                    var storyMapNode_2;
                    if (textNode_1.initiallyDisabled && stepControls && stepControls.next) {
                        stepControls.next.addClass("disabled");
                    }
                    if (textNode_1.questionId) {
                        for (var i = 0; i < _this.story.nodes.length; i++) {
                            if (_this.story.nodes[i].type === "map" && _this.story.nodes[i].questionId === textNode_1.questionId) {
                                storyMapNode_2 = _this.story.nodes[i];
                                break;
                            }
                        }
                    }
                    if (typeof textNode_1.onLeave === "function")
                        _this.onStoryNodeLeaveCallbacks.push(function () { textNode_1.onLeave([textValue_1]); });
                    var textWrapperClasses = "tags-wrapper from-group";
                    var textInputObject = $("<textarea rows=\"3\" class=\"sio-text-input form-control\" id=\"sio-text-input\"></textarea>");
                    textInputObject.on("change", function (event) {
                        if (storyMapNode_2) {
                            storyMapNode_2.result.text = textNode_1.text = textNode_1.tagsPrefix + "::" + $(".sio-text-input").val();
                        }
                    });
                    var textWrapperElement = $("<div class=\"" + textWrapperClasses + "\"></div>");
                    textWrapperElement.append(textInputObject);
                    _this.storyContainer.append(textWrapperElement);
                    if (typeof node.onStart === "function") {
                        node.onStart(_this.storyContainer, function () {
                            _this.storyContainer.fadeIn();
                        });
                    }
                    else {
                        _this.storyContainer.fadeIn();
                    }
                    break;
                case "map":
                    var mapNode = node;
                    if (_this.controls.next)
                        _this.controls.next.addClass("disabled");
                    var htmlRendererContainer = _this.storyContainer.append("<div id=\"emotionmap-htmlrenderer-container\"></div>");
                    _this.htmlRenderer.initializeMap("emotionmap-htmlrenderer-container");
                    _this.htmlRenderer.extendConfiguration({
                        readonly: false
                    });
                    if (mapNode.result && mapNode.result.x && mapNode.result.y) {
                        _this.htmlRenderer.addPoint({
                            value: 1,
                            x: mapNode.result.x,
                            y: mapNode.result.y
                        });
                        if (_this.controls.next)
                            _this.controls.next.removeClass("disabled");
                    }
                    _this.htmlRenderer.onClick(function (xParam, yParam) {
                        if (mapNode.result === undefined)
                            mapNode.result = {};
                        mapNode.result.x = xParam;
                        mapNode.result.y = yParam;
                        if (_this.controls.next)
                            _this.controls.next.removeClass("disabled");
                    });
                    if (typeof mapNode.onResult === "function")
                        mapNode.onResult(_this.htmlRenderer.getData());
                    if (typeof mapNode.onLeave === "function")
                        _this.onStoryNodeLeaveCallbacks.push(function () { mapNode.onLeave(_this.htmlRenderer.getData()); });
                    if (mapNode.questionId !== undefined) {
                        _this.showLoading();
                        _this.tokenHandler(function (token) {
                            _this.backendConnector.loadQuestion(token, mapNode.questionId, function (error, question) {
                                if (error)
                                    throw error;
                                if (!node.title) {
                                    if (!$(".emotionmap-title")) {
                                        _this.storyContainer.append("<div class=\"emotionmap-title\">" + question.phrase + "</div>");
                                    }
                                    else {
                                        $(".emotionmap-title").append(question.phrase);
                                    }
                                }
                                _this.htmlRenderer.setLabels(question.north, question.south, question.west, question.east);
                                _this.hideLoading();
                                if (typeof node.onStart === "function") {
                                    node.onStart(_this.storyContainer, function () {
                                        _this.storyContainer.fadeIn();
                                    });
                                }
                                else {
                                    _this.storyContainer.fadeIn();
                                }
                            });
                        });
                    }
                    else if (mapNode.question) {
                        if (!node.title) {
                            if (!$(".emotionmap-title")) {
                                _this.storyContainer.append("<div class=\"emotionmap-title\">" + mapNode.question.phrase + "</div>");
                            }
                            else {
                                $(".emotionmap-title").append(mapNode.question.phrase);
                            }
                        }
                        _this.htmlRenderer.setLabels(mapNode.question.north, mapNode.question.south, mapNode.question.west, mapNode.question.east);
                        if (typeof node.onStart === "function") {
                            node.onStart(_this.storyContainer, function () {
                                _this.storyContainer.fadeIn();
                            });
                        }
                        else {
                            _this.storyContainer.fadeIn();
                        }
                    }
                    else {
                        throw new Error("A map node needs to define a questionId or a question object!");
                    }
                    break;
                case "view":
                    var viewNode = node;
                    $(".client-logo").show();
                    _this.storyContainer.fadeIn(function () {
                        var jqueryObjects = [];
                        var lastRowIndicator = undefined;
                        $.each(viewNode.texts, function (index, element) {
                            var classes = "emotionmap-view-text";
                            if (viewNode.cssClasses && viewNode.cssClasses[index])
                                classes += " " + viewNode.cssClasses[index];
                            var breakpointHtml = "";
                            if (classes.indexOf("sio-ic-row-") !== -1) {
                                var regex = new RegExp("sio-ic-row-([0-9]*) ", "gi");
                                var result = regex.exec(viewNode.cssClasses[index]);
                                if (result[1]) {
                                    if (lastRowIndicator === undefined || lastRowIndicator !== result[1])
                                        breakpointHtml = "<br>";
                                    lastRowIndicator = result[1];
                                }
                            }
                            jqueryObjects.push($(breakpointHtml + "<span class=\"" + classes + "\" style=\"opacity: 0\">" + element + "</span>"));
                        });
                        _this.storyContainer.append(jqueryObjects);
                        $(jqueryObjects).each(function (fadeInDiv) {
                            if (viewNode.animated)
                                $(this).animate({ 'opacity': 0 }, fadeInDiv * 800, function () {
                                }).animate({ 'opacity': 1 }, 3200);
                            else
                                $(this).css("opacity", 1);
                        });
                    });
                    break;
                case "image":
                    var imageNode = node;
                    _this.storyContainer.append("<img src=\"" + imageNode.imageUrl + "\">");
                    _this.storyContainer.fadeIn();
                    break;
                case "custom":
                    var customNode = node;
                    if (customNode.showLogo === true) {
                        $(".client-logo").show();
                    }
                    if (customNode.initiallyDisabled && stepControls && stepControls.next) {
                        stepControls.next.addClass("disabled");
                    }
                    if (typeof customNode.onStart === "function") {
                        customNode.onStart(_this.storyContainer, function () {
                            _this.storyContainer.fadeIn();
                        });
                    }
                    else if (customNode.selector && $(customNode.selector) && $(customNode.selector).length > 0) {
                        _this.storyContainer.append($(customNode.selector));
                        $(customNode.selector).show();
                        _this.storyContainer.fadeIn();
                        _this.onStoryNodeLeaveCallbacks.push(function () {
                            console.log('[DEBUG]: NEXT: Moving custom element to body', {
                                selector: customNode.selector,
                                element: $(customNode.selector)
                            });
                            $('body').append($(customNode.selector));
                            $(customNode.selector).hide();
                        });
                        _this.onStoryNodeBackCallbacks.push(function () {
                            console.log('[DEBUG]: BACK: Moving custom element to body', {
                                selector: customNode.selector,
                                element: $(customNode.selector)
                            });
                            $('body').append($(customNode.selector));
                            $(customNode.selector).hide();
                        });
                    }
                    else
                        throw new Error("CustomNodes have to implement the onStart function!");
                    if (typeof customNode.onLeave === "function") {
                        _this.onStoryNodeLeaveCallbacks.push(function () { customNode.onLeave(); });
                    }
                    break;
                default:
                    throw new Error("Unknown node type : " + node.type);
            }
        });
    };
    StoryBuilder.prototype.initializeEmptyStory = function () {
        this.story = {
            nodes: []
        };
    };
    StoryBuilder.prototype.renderAnalyticsStep = function (analyticsNode, tagFilter) {
        var _this = this;
        console.log('[DEBUG]: RENDERANALYTICSSTEP', analyticsNode);
        if (!analyticsNode.disableMap) {
            this.storyContainer.empty();
            window.scrollTo(0, 0);
            this.storyContainer.append("<div class=\"story-analytics-map\" id=\"analytics-container\"></div>");
        }
        if (!analyticsNode.disableHeartbeat || !analyticsNode.disableAxesScores) {
            this.storyContainer.append("<div class=\"story-analytics-score\"></div>");
        }
        if (!analyticsNode.disableTags) {
            this.storyContainer.append("<div class=\"story-analytics-charts\"></div>");
        }
        if (!analyticsNode.disableTimeline) {
            this.storyContainer.append("<div class=\"story-analytics-timeline\"></div>");
        }
        var analyticsRenderer = this.htmlRenderer;
        if (!analyticsNode.disableMap) {
            analyticsRenderer.initializeMap("analytics-container");
            analyticsRenderer.extendConfiguration({
                readonly: true
            });
        }
        this.tokenHandler(function (token) {
            _this.backendConnector.loadQuestion(token, analyticsNode.questionId, function (error, question) {
                if (error)
                    throw error;
                if (!analyticsNode.disableMap) {
                    analyticsRenderer.setLabels(question.north, question.south, question.west, question.east);
                }
                _this.hideLoading();
                if (typeof analyticsNode.onStart === "function") {
                    analyticsNode.onStart(_this.storyContainer, function () {
                        _this.storyContainer.fadeIn();
                    });
                }
                else {
                    _this.storyContainer.fadeIn();
                }
                _this.backendConnector.getStats(token, analyticsNode.questionId, tagFilter, function (error, stats) {
                    if (stats) {
                        var scoreContainer = $(".story-analytics-score");
                        if (!analyticsNode.disableHeartbeat || !analyticsNode.disableAxesScores) {
                            if (stats.alltime) {
                                if (!analyticsNode.disableHeartbeat) {
                                    scoreContainer.append("<p><span class=\"story-analytics-stats\">" + stats.alltime.heartbeat + "%</span><br/><span class=\"story-analytics-stats-title\">" + question.name + "</span></p>");
                                }
                                if (!analyticsNode.disableAxesScores) {
                                    scoreContainer.append("<p><span class=\"story-analytics-stats\">" + stats.alltime.avgy + "%</span><br/><span class=\"story-analytics-stats-title\">" + question.yname + " Score</span></p>");
                                    scoreContainer.append("<p><span class=\"story-analytics-stats\">" + stats.alltime.avgx + "%</span><br/><span class=\"story-analytics-stats-title\">" + question.xname + " Score</span></p>");
                                }
                            }
                        }
                        if (!analyticsNode.disableMap && stats.heatmap && stats.heatmap.length > 0) {
                            stats.heatmap.sort(function (a, b) {
                                return parseInt(b.count) - parseInt(a.count);
                            });
                            var mxPoints = [];
                            var highest = parseInt(stats.heatmap[0].count);
                            for (var i = 0; i < stats.heatmap.length; i++) {
                                mxPoints.push({
                                    categoryId: analyticsRenderer.getDefaultCategory().id,
                                    value: stats.heatmap[i].count / highest,
                                    x: stats.heatmap[i].x,
                                    y: stats.heatmap[i].y
                                });
                            }
                            analyticsRenderer.addPoints(mxPoints);
                            analyticsRenderer.renderPoints();
                        }
                        if (!analyticsNode.disableTimeline && stats.timeline) {
                            var dataSerie = _this.aggregateSeries(stats.timeline);
                            var aggSerie = {
                                name: "agg",
                                data: dataSerie
                            };
                            var series = [];
                            series.push(aggSerie);
                            $(".story-analytics-timeline").append("<div class=\"analytics-timeline\"></div>");
                            var xLow = Date.parse(dataSerie[0].x);
                            var divisor = Math.min(5, dataSerie.length);
                            var xHigh = Date.parse(dataSerie[dataSerie.length - 1].x);
                            var sioLineChart = new Chartist.Line(".analytics-timeline", {
                                series: series
                            }, {
                                axisX: {
                                    type: Chartist.FixedScaleAxis,
                                    divisor: divisor,
                                    low: xLow,
                                    high: xHigh + (xHigh - xLow) / divisor,
                                    labelInterpolationFnc: function (value) {
                                        return moment(value).format('MMM D');
                                    }
                                },
                                axisY: {
                                    labelInterpolationFnc: function (value) {
                                        return value + "%";
                                    },
                                    offset: 50
                                },
                                showPoint: false,
                                width: "100%",
                                height: "260px"
                            });
                        }
                        if (tagFilter && tagFilter.length > 0) {
                            var tagFilterLabel = "";
                            var tagFilterParts = tagFilter.split(analyticsNode.tagPrefix + "::");
                            if (tagFilterParts && tagFilterParts.length > 0) {
                                tagFilterLabel = tagFilterParts[1];
                            }
                            $(".story-analytics-charts").append("<div class=\"tag-filter story-analytics-stats-title\">Current filter</div>");
                            $(".story-analytics-charts").append("<div class=\"tag-filter story-analytics-stats\">" + tagFilterLabel + "</div>");
                            $(".story-analytics-charts").append("<button class=\"btn btn-lg btn-primary tag-filter-btn\">Show all</button>");
                            $(".tag-filter-btn").on('click', function (event) {
                                _this.renderAnalyticsStep(analyticsNode, '');
                            });
                        }
                        else if (!analyticsNode.disableTags) {
                            _this.backendConnector.getTagStats(token, analyticsNode.questionId, function (error, tagStats) {
                                if (tagStats && tagStats.length > 0) {
                                    tagStats = tagStats.filter(function (entry) {
                                        return entry.tag.indexOf(analyticsNode.tagPrefix) == 0;
                                    });
                                    tagStats.sort(function (a, b) {
                                        return b.jam_count - a.jam_count;
                                    });
                                    tagStats = tagStats.slice(0, Math.min(tagStats.length, analyticsNode.maxTags));
                                    var pieLabels = [];
                                    var pieSeries = [];
                                    for (var ti = 0; ti < tagStats.length; ti++) {
                                        var label = tagStats[ti].tag;
                                        if (label && label.length > 0) {
                                            label = label.split(analyticsNode.tagPrefix + "::");
                                            if (label.length > 1) {
                                                label = label[1];
                                            }
                                        }
                                        var value = Math.round(100 * tagStats[ti].jam_count / stats.alltime.jam_count);
                                        pieLabels.push(label + " (" + value + "%)");
                                        pieSeries.push({ value: value, meta: tagStats[ti].tag });
                                    }
                                    $(".story-analytics-charts").append("<div class=\"analytics-tag-pie\"></div>");
                                    var sioPieChart = new Chartist.Pie(".analytics-tag-pie", {
                                        labels: pieLabels,
                                        series: pieSeries
                                    }, {
                                        donut: true,
                                        showLabel: true,
                                        width: "100%",
                                        height: "260px"
                                    });
                                    $(".analytics-tag-pie").on('click', function (event) {
                                        if (event && event.target) {
                                            var tagFilter_1 = event.target.getAttribute('ct:meta');
                                            console.log('[DEBUG]: Selected filter', tagFilter_1);
                                            _this.renderAnalyticsStep(analyticsNode, tagFilter_1);
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            });
        });
    };
    StoryBuilder.prototype.addStoryNode = function (storyNode) {
        this.story.nodes.push(storyNode);
    };
    StoryBuilder.prototype.renderNextStep = function () {
        $.each(this.onStoryNodeLeaveCallbacks, function (index, fn) {
            fn();
        });
        this.onStoryNodeBackCallbacks = [];
        this.onStoryNodeLeaveCallbacks = [];
        if (this.hasNextStep()) {
            this.currentStep++;
            this.renderStep(this.currentStep);
        }
    };
    StoryBuilder.prototype.renderPreviousStep = function () {
        if (this.hasPreviousStep()) {
            $.each(this.onStoryNodeBackCallbacks, function (index, fn) {
                fn();
            });
            this.onStoryNodeBackCallbacks = [];
            this.onStoryNodeLeaveCallbacks = [];
            this.currentStep--;
            this.renderStep(this.currentStep);
        }
    };
    StoryBuilder.prototype.hasNextStep = function () {
        return this.story && this.currentStep < (this.story.nodes.length - 1);
    };
    StoryBuilder.prototype.hasPreviousStep = function () {
        return this.currentStep > 0;
    };
    StoryBuilder.prototype.addControls = function (controls) {
        this.controls = controls;
        this.mapControls();
    };
    StoryBuilder.prototype.addControlsById = function (controls) {
        if (controls.cancel) {
            this.controls.cancel = $("#" + controls.cancel);
            if (this.controls.cancel.length === 0)
                console.log("Could not find controls with ID '" + controls.cancel + "'!");
        }
        if (controls.next) {
            this.controls.next = $("#" + controls.next);
            if (this.controls.next.length === 0)
                console.log("Could not find controls with ID '" + controls.next + "'!");
        }
        if (controls.previous) {
            this.controls.previous = $("#" + controls.previous);
            if (this.controls.previous.length === 0)
                console.log("Could not find controls with ID '" + controls.previous + "'!");
        }
        if (controls.submit) {
            this.controls.submit = $("#" + controls.submit);
            if (this.controls.submit.length === 0)
                console.log("Could not find controls with ID '" + controls.submit + "'!");
        }
        if (controls.stepsCounter) {
            this.controls.stepsCounter = $("#" + controls.stepsCounter);
            if (this.controls.stepsCounter.length === 0)
                console.log("Could not find controls with ID '" + controls.stepsCounter + "'!");
        }
        this.mapControls();
    };
    StoryBuilder.prototype.mapControls = function () {
        var _this = this;
        if (this.controls.next)
            this.controls.next.on("click", function () { if (!_this.controls.next.hasClass('disabled')) {
                _this.renderNextStep();
            } });
        if (this.controls.previous)
            this.controls.previous.on("click", function () { _this.renderPreviousStep(); });
        if (this.controls.cancel)
            this.controls.cancel.on("click", function () {
                _this.reset();
                _this.start(_this.getStepURLParameter(0));
            });
        if (this.controls.submit)
            this.controls.submit.on("click", function () {
                if (!_this.controls.submit.hasClass('disabled')) {
                    _this.sendResults(function () {
                        _this.reset();
                        _this.start(_this.getStepURLParameter(0));
                    });
                }
            });
    };
    StoryBuilder.prototype.getStepURLParameter = function (defaultStep) {
        if (defaultStep === void 0) { defaultStep = 0; }
        var urlStep = parseInt(Utils.getParameterByName("step"));
        if (typeof urlStep !== "number" || isNaN(urlStep))
            return defaultStep;
        else
            return urlStep;
    };
    StoryBuilder.prototype.showLoading = function () {
        this.htmlRenderer.showLoading(this.storyContainer.attr("id"), true);
    };
    StoryBuilder.prototype.hideLoading = function () {
        this.htmlRenderer.showLoading(this.storyContainer.attr("id"), false);
    };
    StoryBuilder.prototype.addTokenHandler = function (tokenHandler) {
        this.tokenHandler = tokenHandler;
    };
    StoryBuilder.prototype.setGlobalTags = function (tags) {
        this.globalTags = tags;
    };
    StoryBuilder.prototype.getGlobalTags = function () {
        return this.globalTags;
    };
    StoryBuilder.prototype.addGlobalTags = function (tags) {
        this.globalTags = this.globalTags.concat(tags);
    };
    StoryBuilder.prototype.clearGlobalTags = function () {
        this.globalTags = [];
    };
    StoryBuilder.prototype.setGlobalKey = function (key) {
        this.globalKey = key;
    };
    StoryBuilder.prototype.setGlobalTagPrefix = function (prefix) {
        this.globalTagPrefix = prefix;
    };
    StoryBuilder.prototype.skipStepsInCounter = function (stepsToSkip) {
        this.stepsToSkipInCounter = stepsToSkip;
    };
    StoryBuilder.prototype.sendResults = function (successCallback) {
        var _this = this;
        this.tokenHandler(function (token) {
            $.each(_this.story.nodes, function (index, node) {
                if (node.type === "map") {
                    var mapNode = node;
                    var result = {
                        key: mapNode.result.key,
                        questionId: mapNode.questionId,
                        x: mapNode.result.x,
                        y: mapNode.result.y,
                        tags: mapNode.result.tags,
                        text: mapNode.result.text
                    };
                    if (!result.key)
                        result.key = _this.globalKey;
                    if (!(result.tags instanceof Array))
                        result.tags = [];
                    result.tags = result.tags.concat(_this.globalTags);
                    if (result.text && result.text.length > 0) {
                        result.tags = result.tags.concat(result.text);
                    }
                    if (_this.globalTagPrefix !== undefined) {
                        $.each(result.tags, function (index, tag) {
                            tag = _this.globalTagPrefix + "::" + tag;
                        });
                    }
                    _this.backendConnector.addRecord(token, result, function (error, response) {
                        if (error)
                            console.log(error);
                        _this.resultSubmitted = true;
                    });
                }
            });
        });
    };
    return StoryBuilder;
}());

exports.BackendConnector = BackendConnector;
exports.HTMLRenderer = HTMLRenderer;
exports.StoryBuilder = StoryBuilder;
exports.Utils = Utils;

}((this.sensationio = this.sensationio || {}),Chartist,moment));
//# sourceMappingURL=sensation-webplugin.js.map
