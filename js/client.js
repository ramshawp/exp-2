$(document).ready(function () {

    //-------------------------------------------------------------------------
    // Configuration BEGIN
    //-------------------------------------------------------------------------
    
    var clientId = sensationio.Utils.getParameterByName("cid") || "b8m9dvnPJ4NnCOntMf8GQw3P2996VJfl";
    var tenantId = sensationio.Utils.getParameterByName("tid") || "vx2650101df26a14105aef355e6f89aa1ba";
    var storyId = sensationio.Utils.getParameterByName("sid") || "default-vaya-exp-story";

    // Remember to 
    // 1. add this to your tenant's redirect_uris
    // 2. add the base path to sioauth.html
    //var redirectUri = "http://exp.sensation.io/sioauth.html";
    var redirectUri = "http://f91db27d.ngrok.io/sioauth.html";

    var welcomeTitle = "SIO SANDBOX";

    //-------------------------------------------------------------------------
    // Configuration END
    //-------------------------------------------------------------------------



    if (bowser.msie && bowser.version <= 10  ) {
        alert('This browser is not supported. Please use IE 11, Chrome, Firefox or Safari.');
        return;
    }

    // var x, y, selectedTags, token;
    var token;

    console.log('[DEBUG]: Loading web story', {
        clientId: clientId,
        tenantId: tenantId,
        storyId: storyId,
        redirectUri: redirectUri
    });

    $(".welcomeTitle").html(welcomeTitle);

    var backendConnector = new sensationio.BackendConnector();
    backendConnector.setHost("https://api.sensation.io");

    var htmlRenderer = new sensationio.HTMLRenderer();

    var defaultCategory = htmlRenderer.getDefaultCategory();
    defaultCategory.foregroundColor = {
        r: 104,
        g: 120,
        b: 110
    };

    htmlRenderer.extendConfiguration({
        "categories": {
            "defaultCategory": defaultCategory
        },
        submitOnClick: false,
        emotionMapSquareSize: 26
    });

    var storyBuilder = new sensationio.StoryBuilder($("#container"), htmlRenderer, backendConnector);

    storyBuilder.addControlsById({
        "cancel": "cancelBtn",
        "next": "nextBtn",
        "previous": "previousBtn",
        "stepsCounter": "stepsCounter",
        "submit": "submitBtn"
    });

    function getToken(callback) {
        if (token)
            callback(token);
        else {
            var parameterToken = sensationio.Utils.getParameterByName('siotkn');
            if (parameterToken && parameterToken.length > 0) {
                token = parameterToken;
                callback(token);
            } else {
                backendConnector.authenticateApp(clientId, tenantId, redirectUri, ["basic.read", "basic.write", "basic.analytics"], function (err, sioToken) {
                    if (err)
                        console.error(err);
                    else {
                        token = sioToken;
                        callback(token);
                    }
                });
            }
        }
    }
    storyBuilder.addTokenHandler(getToken);
    storyBuilder.loadStoryFromURL("https://api.sensation.io/v2/me/configs/" + storyId, function () {
        var startStep = parseInt(sensationio.Utils.getParameterByName("step"));
        if (typeof startStep === "number" && !isNaN(startStep)){
            storyBuilder.start(startStep);
        }
        else{
            storyBuilder.start();
        }
    });
});
